%undefine _hardened_build
%global  gcc_major  7.3.0

Name:    libtool
Version: 2.5.4
Release: 1
License: GPL-2.0-or-later AND LGPL-2.0-or-later and GFDL-1.3-or-later
Summary: The GNU Portable Library Tool
URL:     https://www.gnu.org/software/libtool/
Source0: https://ftp.gnu.org/gnu/libtool/libtool-%{version}.tar.xz

Patch0:  libtool-2.5.3-rpath.patch
Patch1:  libtool-2.4.6-disable-lto-link-order2.patch

Requires: gcc(major),autoconf, automake, sed, tar, findutils

BuildRequires: texinfo autoconf automake help2man
BuildRequires: libstdc++-devel gcc-gfortran gcc gcc-c++

%description
GNU libtool is a generic library support script.
Libtool hides the complexity of using shared libraries behind a consistent, portable interface.

%package ltdl
Summary: Runtime libraries for GNU Libtool Dynamic Module Loader
Provides: %{name}-libs = %{version}-%{release}
License: LGPL-2.1-or-later

%description ltdl
The libtool-ltdl package contains the GNU Libtool Dynamic Module Loader, a
library that provides a consistent, portable interface which simplifies the
process of using dynamic modules.

%package   devel
Summary:   Tools needed for development using the GNU Libtool Dynamic Module Loader
License:   LGPL-2.0-or-later
Requires:  automake
Requires:  %{name}-ltdl = %{version}-%{release}
Provides:  %{name}-ltdl-devel = %{version}-%{release}
Obsoletes: %{name}-ltdl-devel < %{version}-%{release}

%description devel
Static libraries and header files for development with ltdl.

%package_help

%prep
%autosetup -n libtool-%{version} -p1
# Fix libtool to pass -Wl,xxx options before libraries
sed -i \
   -e 's,$libobjs $deplibs $compiler_flags,$compiler_flags $libobjs $deplibs,' \
   -e 's,$predep_objects $libobjs $deplibs $postdep_objects $compiler_flags,$compiler_flags $predep_objects $libobjs $deplibs $postdep_objects,' \
 m4/libtool.m4

%build
autoreconf -v
export CC=gcc
export CXX=g++
export F77=gfortran
export CFLAGS="$RPM_OPT_FLAGS -fPIC"
export FFLAGS=$(echo "$RPM_OPT_FLAGS -I/usr/lib64/gfortran/modules"| sed 's/-fstack-protector-strong/ /g')
export FCFLAGS=$(echo "$RPM_OPT_FLAGS -I/usr/lib64/gfortran/modules"| sed 's/-fstack-protector-strong/ /g')
%ifarch x86_64
export FFLAGS="$RPM_OPT_FLAGS -fPIE"
export FCFLAGS="$RPM_OPT_FLAGS -fPIE"
%endif
%ifarch riscv64
export FFLAGS="$RPM_OPT_FLAGS -fPIC"
export FCFLAGS="$RPM_OPT_FLAGS -fPIC"
%endif

%configure

%make_build CUSTOM_LTDL_CFLAGS="%_hardening_cflags" CUSTOM_LTDL_LDFLAGS="%_hardening_ldflags"

%check
%make_build check VERBOSE=yes

%install
%make_install
%delete_la_and_a
rm -f %{buildroot}%{_infodir}/dir

%files
%license COPYING
%doc AUTHORS NEWS THANKS TODO ChangeLog*
%{_bindir}/libtool
%{_bindir}/libtoolize
%{_datadir}/aclocal/*.m4
%dir %{_datadir}/libtool
%{_datadir}/libtool/build-aux

%files ltdl
%license libltdl/COPYING.LIB
%{_libdir}/libltdl.so.*

%files devel
%license libltdl/COPYING.LIB
%doc libltdl/README
%{_datadir}/libtool/COPYING.LIB
%{_datadir}/libtool/Makefile.am
%{_datadir}/libtool/Makefile.in
%{_datadir}/libtool/README
%{_datadir}/libtool/aclocal.m4
%{_datadir}/libtool/config-h.in
%{_datadir}/libtool/configure
%{_datadir}/libtool/configure.ac
%{_datadir}/libtool/libltdl
%{_datadir}/libtool/loaders
%{_datadir}/libtool/lt__alloc.c
%{_datadir}/libtool/lt__argz.c
%{_datadir}/libtool/lt__dirent.c
%{_datadir}/libtool/lt__strl.c
%{_datadir}/libtool/lt_dlloader.c
%{_datadir}/libtool/lt_error.c
%{_datadir}/libtool/ltdl.c
%{_datadir}/libtool/ltdl.h
%{_datadir}/libtool/ltdl.mk
%{_datadir}/libtool/slist.c
%{_includedir}/ltdl.h
%{_includedir}/libltdl
%{_libdir}/libltdl.so

%files help
%doc README
%{_infodir}/libtool.info*
%{_mandir}/man1/libtool.1*
%{_mandir}/man1/libtoolize.1*

%changelog
* Thu Nov 21 2024 Funda Wang <fundawang@yeah.net> - 2.5.4-1
- update to 2.5.4

* Tue Oct 08 2024 Funda Wang <fundawang@yeah.net> - 2.5.3-1
- update to 2.5.3

* Thu Sep 26 2024 Funda Wang <fundawang@yeah.net> - 2.4.7-4
- pass -Wl,xxx options before libraries
- disable LTO for link-order2 test

* Tue Jun 06 2023 laokz <zhangkai@iscas.ac.cn> - 2.4.7-3
- add -fPIC to fortran flags for riscv

*Thu Feb 02 2023 renhongxun <renhongxun@h-partners.com> - 2.4.7-2
- backport: tests/link-order.at: avoid warning and test failure with GNU grep 3.8

* Thu Nov 03 2022 renhongxun <renhongxun@h-partners.com> - 2.4.7-1
- upgrade version to 2.4.7

* Tue Apr 26 2022 renhongxun <renhongxun@h-partners.com> - 2.4.6-35
- udpate license from GFDL to GFDL-1.3-or-later

* Fri Jul 23 2021 yuanxin <yuanxin24@huawei.com> - 2.4.6-34
- remove BuildRequires gdb

* Thu Aug 20 2020 tianwei <tianwei12@huawei.com> - 2.4.6-33
- fixbug testcase fail for gfortan

* Thu Mar 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.4.6-32
- add necessary BuildRequires

* Mon Jan 20 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.4.6-31
- fixbug in wrong dependency of kernel-devel

* Wed Jan 8 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.4.6-30
- format patches

* Thu Sep 5 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.4.6-29
- Package init

* Sun Feb 24 2019 zoujing <zoujing13@huawei.com> - 2.4.6-28
- Type:NA
- ID:NA
- SUG:NA
- DESC: change gcc version number on aarch64

